<?php include 'navbar.php';

	if(isset($_POST['nazwaNP'])){
		$nazwa = $_POST['nazwaNP'];
		$cena = $_POST['cenaNP'];
		$opis = $_POST['opisNP'];
		$url = $_POST['urlNP'];
		unset($_POST['nazwaNP']);
		unset($_POST['cenaNP']);
		unset($_POST['opisNP']);
		unset($_POST['urlNP']);

		$sql="INSERT INTO `product` (`Id`, `Nazwa`, `Cena`, `Opis`, `URLtoIMG`) 
			VALUES (NULL,'$nazwa', $cena, '$opis','$url')";
		$result = @mysqli_query($con,$sql);
		// echo $result;
	}

	if(isset($_POST['delP'])){
		$delID = $_POST['delP'];
		unset($_POST['delP']);
		$sql="DELETE FROM `product` WHERE Id = $delID";
		// echo "sql: " . $sql;
		$result = @mysqli_query($con,$sql);
		// echo $result;
	}
?>
<html>
<head>
	<meta charset="UTF-8">
	<style type="text/css">
		.card{
			width: 20rem;
			display: inline-block;					
			cursor: pointer;	
		}
		.card img{
		    position: relative;
    		width: 100%;
		}

		.my-temp li{
			cursor: pointer;
		    background-color: rgba(120, 120, 120, 0.6);
		    border: 1px solid rgba(180, 180, 180, 0.5);
		    color: rgba(180, 180, 180, 0.5);
		    transition: all 1s ease-in;
		}

		.my-temp img{
			z-index: -1;
		}

		.my-temp:hover li{
			background-color: rgba(120, 120, 120, 0.2);
			height: 100%;
		}

		.desc{
			transition: all 0.3s ease-in;
		}

		.desc:hover {		
			height: 150px !important;
		}


		.my-temp:hover .glyphicon{
			color: rgba(120, 120, 120, 0.6);
		}

		.my-temp .glyphicon {
		    position: absolute;
		    top: 80%;
		    left: 50%;
		    color: white;
		    font-size: 200%;
		    transform: translateX(-50%);
		    transition: all 1s ease-in;
		}
		.my-temp-new{
			display: none;
		}
	</style>
</head>
<body>
	<div id="root" class="container-fluid">			
		<?php
			if(isset($_POST['searchElem'])){			
				$elQuery =  mysqli_real_escape_string( $con, $_POST['searchElem']);
				unset($_POST['searchElem']);
				$sql = "SELECT * FROM product WHERE Nazwa LIKE '%$elQuery%'";
			} else {
				$sql = "SELECT * FROM product";
			}
			$result = @mysqli_query($con, $sql) or die("Błąd wykonania instrukcji SELECT");
			while ($row = mysqli_fetch_array($result)){ ?>
			<div class="card my-temp-show">
				<ul class="list-group list-group-flush">
					<li class="list-group-item" style="height: 190px;"><img class="card-img-top" src="<?php echo $row['URLtoIMG']; ?>" alt="Card image cap"></li>
					<li class="list-group-item desc" style="height: 100px;">
						<h4 class="card-title"><?php echo $row['Nazwa']; ?></h4>
						<p class="card-text"><?php echo $row['Opis']; ?></p>
					</li>
					<li class="list-group-item"><?php echo $row['Cena'] . ' PLN'; ?></li>
					<li class="list-group-item">
						<!-- <a href="#" class="card-link">Modyfikuj</a> -->
						<form id="delForm<?php echo $row['Id']; ?>" action="catalog.php" method="POST">
							<input type="hidden" name="delP" value="<?php echo $row['Id']; ?>">
							<a onclick="q$('#delForm<?php echo $row['Id']; ?>').submit();">Usuń</a>					
						</form>
					</li>
				</ul>
			</div>
		<?php	}		?>
		<div class="card my-temp-new" onclick="">
			<form id='NPform' action="catalog.php" method="POST">
				<ul class="list-group list-group-flush">
					<li class="list-group-item" style="height: 190px;">					
						<input type="text" class="form-control" name="urlNP" placeholder="URL to Image.." >
					</li>
					<li class="list-group-item" style="height: 100px;">
						<input type="text" class="form-control" name="nazwaNP" placeholder="Nazwa.."  required="true">
						<input type="text" class="form-control" name="opisNP" placeholder="Opis.." style="margin-top: 10px;"  >
					</li>
					<li class="list-group-item" style="padding: 3px 15px;">
						<input type="number" class="form-control" name="cenaNP" placeholder="Cena.."  required="true">
					</li>
					<li class="list-group-item">
						<input type="hidden" name="np">	
    					<a href="#" onclick="q$('#NPform').submit();">Zapisz</a>					
					</li>
				</ul>
			</form>
		</div>
		<div class="card my-temp" onclick="q$('.my-temp-new').css('display', 'inline-block'); q$(this).css('display', 'none');">
			<ul class="list-group list-group-flush">
				<li class="list-group-item" style="height: 190px;">
					<span class="glyphicon glyphicon-plus"></span>
					<img class="card-img-top" src="http://hdimages.org/wp-content/uploads/2017/03/placeholder-image4.jpg" alt="Card image cap">
				</li>
				<li class="list-group-item" style="height: 100px;">
					<h4 class="card-title">Nazwa..</h4>
					<p class="card-text">Opis..</p>
				</li>
				<li class="list-group-item">Cena..</li>
				<li class="list-group-item">
					&nbsp;
				</li>
			</ul>
		</div>
	</div>
</body>
</html>
