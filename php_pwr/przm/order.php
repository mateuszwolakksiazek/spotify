<?php include 'navbar.php';	?>
<html>
<head>	
	<style type="text/css">
		.prd-container{
			position: absolute;
			width: 150%;
			margin-top: 50px;
			height: 400px;
		}

		.prd{
			background-color: lightgoldenrodyellow;
			width: 250px;
			height: 250px;
			border-radius: 2px;
			margin: 3px;
			display: inline-block;
			cursor: pointer;
			top: 100;
			transition: 2s all ease-in;
		}
		.prd:hover{
			width: 300px;
			height: 300px;
		}
	</style>
</head>
<body>
	<div id="root" class="container-fluid">	
		<div class="prd-container">
			<span class="prd"></span>
		</div>
	</div>
</body>
</html>
