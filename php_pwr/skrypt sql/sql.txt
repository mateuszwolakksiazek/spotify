CREATE TABLE `Odtwarzanie_utworu` (
  `id_sesja` INTEGER, 
  `id_utwor` INTEGER,
  KEY `FK` (`id_sesja`, `id_utwor`)
);

CREATE TABLE `Album_has_wykonawca` (
  `id_wykonawca` INTEGER,
  `id_album` INTEGER,
  `numer` INTEGER,
  KEY `FK` (`id_wykonawca`, `id_album`)
);

CREATE TABLE `Sesja` (
  `id_Sesja` SERIAL,
  `id_uzytkownik` INTEGER, 
  `data` DATE, 
  `godzina` TIME,
  PRIMARY KEY (`id_Sesja`),
  KEY `FK` (`id_uzytkownik`)
);

CREATE TABLE `Zespol_has_osoba` (
  `id_zespol` INTEGER,
  `id_osoba` INTEGER,
  `aktualnie` BOOL,
  `od` DATE,
  `do` DATE,
  KEY `FK` (`id_zespol`, `id_osoba`)
);

CREATE TABLE `Gatunek` (
  `id_gatunek` SERIAL,
  `nazwa_gatunku` varchar(10), 
  PRIMARY KEY (`id_gatunek`)
);

CREATE TABLE `Album_has_utwor` (
  `id_utwor` INTEGER,
  `id_album` INTEGER,
  `numer` INTEGER,
  KEY `FK` (`id_utwor`, `id_album`)
);

CREATE TABLE `Osoba` (
  `id_osoba` SERIAL,
  `imie` VARCHAR(45),
  `nazwisko` VARCHAR(45),
  `pseudonim` VARCHAR(45),
  `data_urodzenia` DATE,
  `plec` BOOL,
  PRIMARY KEY (`id_osoba`)
);

CREATE TABLE `Utwor_has_wykonawca` (
  `id_utwor` INTEGER,
  `id_wykonawca` INTEGER,
  KEY `FK` (`id_utwor`, `id_wykonawca`)
);

CREATE TABLE `Album` (
  `id_album` SERIAL,
  `id_gatunek` INTEGER,
  `tytul` VARCHAR(255),
  `ilosc_utworow` INTEGER, 
  `rok_wydania` varchar(4), 
  `dlugosc_albumu` TIME, 
  `jedenwykonawca` BOOL,
  PRIMARY KEY (`id_album`),
  KEY `FK` (`id_gatunek`)
);

CREATE TABLE `Uzytkownik` (
  `id_uzytkownik` SERIAL,
  `login` varchar(16), 
  `haslo` varchar(32), 
  `poprz_haslo` varchar(32),
  `email` varchar(32), 
  `administrator` BOOL,
  PRIMARY KEY (`id_uzytkownik`)
);

CREATE TABLE `Wykonawca_rodzaj` (
  `id_wykonawca_rodzaj` SERIAL,
  `nazwa` VARCHAR(45),
  `sortowanie` INTEGER,
  PRIMARY KEY (`id_wykonawca_rodzaj`)
);

CREATE TABLE `Utwor` (
  `id_utwor` SERIAL,
  `tytul` varchar(255), 
  `id_album` INTEGER, 
  `rok_powstania` char(4), 
  `czas_trwania` TIME, 
  `mp3` BLOB,
  `liczba_wyswietlen` INTEGER, 
  PRIMARY KEY (`id_utwor`),
  KEY `FK` (`id_album`)
);

CREATE TABLE `Wykonawca` (
  `id_wykonawca` SERIAL,
  `id_wykonawca_rodzaj` INTEGER,
  `id_zespol` INTEGER,
  `id_osoba` INTEGER,
  `osobazespol` BOOL,
  PRIMARY KEY (`id_wykonawca`),
  KEY `FK` (`id_wykonawca_rodzaj`, `id_zespol`, `id_osoba`)
);

CREATE TABLE `Zespol` (
  `id_zespol` SERIAL,
  `nazwa` varchar(30),
  PRIMARY KEY (`id_zespol`)
);

