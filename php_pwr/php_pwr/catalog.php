<?php include 'interfejs.php';

?>
<html>
<head>
	<meta charset="UTF-8">
	<script type="text/javascript">
		q$ = jQuery.noConflict();

		var x = document.getElementById("myAudio"); 

		function handleAudio(input, audioId) {
			var audio = document.getElementById(audioId);
			if(input.value === "PLAY") {
		    	audio.play(); 
				input.value = "PAUSE";
			} 	else {
				audio.pause(); 
				input.value = "PLAY";
			}	 
		} 

		
	</script>
	<style type="text/css">
		.card{
			width: 20rem;
			display: inline-block;					
			cursor: pointer;	
		}
		.card img{
		    position: relative;
    		width: 100%;
		}

		.my-temp li{
			cursor: pointer;
		    background-color: rgba(120, 120, 120, 0.6);
		    border: 1px solid rgba(180, 180, 180, 0.5);
		    color: rgba(180, 180, 180, 0.5);
		    transition: all 1s ease-in;
		}

		.my-temp img{
			z-index: -1;
		}

		.my-temp:hover li{
			background-color: rgba(120, 120, 120, 0.2);
			height: 100px;
		}

		.desc{
			transition: all 0.3s ease-in;
		}

		.desc:hover {		
			height: 150px !important;
		}


		.my-temp:hover .glyphicon{
			color: rgba(120, 120, 120, 0.6);
		}

		.my-temp .glyphicon {
		    position: absolute;
		    top: 80%;
		    left: 50%;
		    color: white;
		    font-size: 200%;
		    transform: translateX(-50%);
		    transition: all 1s ease-in;
		}
		.my-temp-new{
			display: none;
		}

		





		#image_div .img_wrapper{
		 width:180px;
		 position:relative;
		 display:inline-block;
		}

		#image_div .img_wrapper img{
		 width:100%;
		}

		#image_div .img_wrapper:hover img{
		 -webkit-filter: blur(1.7px);
		}

		#image_div .img_wrapper span{
		 display:none;
		 position:absolute;
		 top:65px;
		 left:30px;
		}

		#image_div .img_wrapper:hover span{
		 display:table-cell;
		}

		#image_div .img_wrapper span input[type="button"]{
		 width:120px;
		 height:40px;
		 background-color:#00BFFF;
		 border:none;
		 color:white;
		 font-weight:bold;
		 font-size:17px;
		}


	</style>
</head>
<body>
	<div id="root" class="container-fluid">			
		<?php
			if(isset($_POST['searchElem'])){			
				$elQuery =  mysqli_real_escape_string( $con, $_POST['searchElem']);
				echo $_POST['searchElem'];
				unset($_POST['searchElem']);
				$sql = "SELECT * FROM utwor WHERE id_album LIKE '%$elQuery%'";
			} else {
				$sql = "SELECT * FROM utwor";
			}
			$result = @mysqli_query($con, $sql) or die("Błąd wykonania instrukcji SELECT");
			while ($row = mysqli_fetch_array($result)){ ?>
			<div class="card my-temp-show">
				<ul class="list-group list-group-flush">
					<li class="list-group-item" style="height: 190px;">
						<div id="wrapper">
							<div id="image_div">
								<p class="img_wrapper">
									<img class="card-img-top" src="<?php echo $row['URLtoIMG']; ?>" alt="Card image cap">
									<span><input type="button" value="PLAY" onclick="handleAudio(this, 'audio-file-<?php echo $row['id_utwor']; ?>')"></span>
								</p>
							</div>
						</div>
					</li>
					<li class="list-group-item desc" style="height: 100px;">
						<h4 class="card-title"><?php echo $row['tytul']; ?></h4>
						<p class="card-text"><?php echo $row['rok_powstania']; ?></p>
					</li>
					<li class="list-group-item"><?php echo $row['czas_trwania']; ?></li>
					<li class="list-group-item">
						<audio id="audio-file-<?php echo $row['id_utwor']; ?>">
							  <source src="<?php echo $row['PATHtoFILE']; ?>" type="audio/mpeg">
						</audio>
					</li>
					<li class="list-group-item">
						<!-- <a href="#" class="card-link">Modyfikuj</a> -->
						<form id="delForm<?php echo $row['Id']; ?>" action="catalog.php" method="POST">
							<input type="hidden" name="delP" value="<?php echo $row['Id']; ?>">
							<a onclick="q$('#delForm<?php echo $row['Id']; ?>').submit();">Usuń</a>					
						</form>
					</li>
				</ul>
			</div>
		<?php	}		?>
		<div class="card my-temp-new" onclick="">
			<form id='NPform' action="addTrack.php" method="POST">
				<ul class="list-group list-group-flush">
					<li class="list-group-item" style="height: 190px;">					
						<input type="text" class="form-control" name="urlNP" placeholder="Źródło okładki.." >
						<input type="text" class="form-control" name="pathNP" placeholder="Ścieżka mp3.." >
					</li>
					<li class="list-group-item" style="height: 100px;">
						<input type="text" class="form-control" name="tytulNP" placeholder="Tytul.."  required="true">
						<input type="text" class="form-control" name="albumNP" placeholder="ID albumu.." style="margin-top: 10px;"  >
					</li>
					<li class="list-group-item" style="padding: 3px 15px;">
						<input type="number" class="form-control" name="rokNP" placeholder="Rok powstania.."  required="true">
						<input type="number" class="form-control" name="czasNP" placeholder="Czas trwania.."  required="true">
					</li>
					<li class="list-group-item">
						<input type="hidden" name="np">	
    					<a href="#" onclick="q$('#NPform').submit();">Zapisz</a>					
					</li>
				</ul>
			</form>
		</div>
		<div class="card my-temp" onclick="q$('.my-temp-new').css('display', 'inline-block'); q$(this).css('display', 'none');">
			<ul class="list-group list-group-flush">
				<li class="list-group-item" style="height: 190px;">
					<span class="glyphicon glyphicon-plus"></span>
					<img class="card-img-top" src="http://hdimages.org/wp-content/uploads/2017/03/placeholder-image4.jpg" alt="Card image cap">
				</li>
				<li class="list-group-item" style="height: 100px;">
					<h4 class="card-title">Nazwa..</h4>
					<p class="card-text">Opis..</p>
				</li>
				<li class="list-group-item">Cena..</li>
				<li class="list-group-item">
					&nbsp;
				</li>
			</ul>
		</div>
	</div>
</body>
</html>
