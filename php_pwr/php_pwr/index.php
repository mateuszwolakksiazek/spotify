﻿<?php

	session_start();

/*	if(isset($_GET['signOut'])){
		unset($_SESSION['login']);
		unset($_SESSION['pass']);
	} 
	*/
	if (
		(isset($_SESSION["login"])) && ($_SESSION["login"]==true) && 
		(isset($_SESSION["password"])) && ($_SESSION["password"]==true)
	) {
		header("Location: interfejs.php");
		exit();
	}

?>
<!--
<!DOCTYPE HTML>
<html lang="pl">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>Portal muzyczny</title>
</head>

<body>
	
	Portal muzyczny<br /><br />
	
	<a href="rejestracja.php">Rejestracja - zarejestruj się w naszym portalu</a>
	<br /><br />
	
	<form action="zaloguj.php" method="post">
	
		Login: <br /> <input type="text" name="login" /> <br />
		Hasło: <br /> <input type="password" name="haslo" /> <br /><br />
		<input type="submit" value="Zaloguj się" />
	
	</form>

	<br><br>

	<a href="administrator.php">Zaloguj się jako pracownik</a>
	

</body>
</html>-->


<html>
<head>
	<title>e-Shop PHP</title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<script type="text/javascript">
		q$ = jQuery.noConflict();

		

		
		q$(document).ready(function() {
		  	var errorVal = q$('#error-input').val();
		  	if(errorVal !== '') {
				q$("#error-display").text(errorVal);	  	
		  		q$('#openModal').click();
		  	}

		  	q$('#close-button').on('click', function() {
		        q$('#error-input').val('');
			});

			q$('#close-button-2').on('click', function() {
	        	q$('#error-input').val('');
			});
		 
		});
	</script>
	<style type="text/css">
		body{
			overflow-x: hidden;
		}

		.loginForm{
			margin: auto;
		    padding: 50px;
		    max-width: 450px;
		    background-color: white;
		    margin-top: 25px;
		    border-radius: 5px;
		    z-index: 2;
		}

		.loginForm .btn, h3 {
			color: #4fcf62;
		}

		.loginForm .glyphicon {
			left: -54%;
			top: 30px;
		}

		.backEffect{
		    position: absolute;
		    margin: auto;
		    left: 0;
		    right: 0;
		    padding: 100px 210px;
		    max-width: 350px;
		    margin-top: 120px;
		    border-radius: 10px;
		    z-index: -1;
		    box-shadow: 0 0 100px 5px #55cf4f;
		}
	</style>
</head>
<body>
	<div id="root" class="container-fluid">			
		<div class="backEffect"></div>
		<div class="loginForm">
			<center>
				<h2>Portal muzyczny</h2>
				<h3> LOGOWANIE </h3>
				<br/>
				<form action="zaloguj.php" method="POST">
				    <!--<?php if(isset($_GET['wrongLogin']) ){ ?>
				    	<span style="color:red;">Błędny Login lub Hasło. </span>
				    <?php } ?>-->
					<div class="form-group">
					    <label class="sr-only" for="login">Login</label>
					    <span class="glyphicon glyphicon-user"></span>
					    <input type="text" class="form-control" id="login" name="login" placeholder="Login"  required="true">
					</div>
					<div class="form-group">
					    <label class="sr-only" for="password">Hasło</label>
					    <span class="glyphicon glyphicon-lock"></span>
					    <input type="password" class="form-control" id="password" name="password" placeholder="Hasło" required="true">
					</div>									
					<a href="/rejestracja.php" class="btn btn-default" />Rejestracja</a>
					<!-- <input class="btn btn-default" type="submit" name="zaloguj" value="Rejestracja" onclick="document.location.href='/rejestracja.php"> -->
					<input class="btn btn-default" type="submit" name="zaloguj" value="Zaloguj">
				</form>

				
				<button id="openModal" style="display: none;" type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>

					<!-- Modal -->
				<div class="modal fade" id="myModal" role="dialog">
				    <div class="modal-dialog">
				    
				      <!-- Modal content-->
				      <div class="modal-content">
				        <div class="modal-header">
				          <button id="close-button-2" type="button" class="close" data-dismiss="modal">&times;</button>
				          <h4 class="modal-title">Wystąpił błąd</h4>
				        </div>
				        <div class="modal-body">
				        	<input id="error-input" type="text" name="result" style="display: none;" value = "<?php echo (isset($_SESSION["error"]))?$_SESSION["error"]:''; unset($_SESSION["error"]);?>">

				          <p id="error-display" class="errorP"/>
				        </div>
				        <div class="modal-footer">
				          <button id="close-button" type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
				        </div>
				      </div>
				      
				    </div>
				</div>

			</center>
		</div>		
	</div>
</body>
</html>