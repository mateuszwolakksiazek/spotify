﻿<?php
    session_start();

    $con = @new mysqli("localhost", "root", "", "projekt");
	
    $response = array();
    $response["success"] = false;
	if($con->connect_errno!=0)
	{
		$response["success"] = false;
		echo "Wystąpił błąd, przepraszamy.";
	}
	else
	{

		if(!isset($_SESSION["login"])){
			if(!isset($_POST["login"])){
				header("Location: /index.php");
			} else {
	
				$login = $_POST["login"];
				$password = $_POST["password"];
				
				$login = htmlentities($login, ENT_QUOTES, "UTF-8");
				$password = htmlentities($password, ENT_QUOTES, "UTF-8");
				
				if($result = $con->query(sprintf("SELECT * FROM Uzytkownik WHERE Login = '%s'", mysqli_real_escape_string($con, $login))))
				{
					$howManyUsers = $result->num_rows;

				
					if($howManyUsers> 0)
					{
						$line = $result->fetch_assoc();
						//if(md5($password) == $line['haslo'])
						if($password == $line['haslo'])
						{
							// store login and password in session
							$_SESSION["login"] = $_POST["login"];
							$_SESSION["password"] = $_POST["password"];
						  	header('Location: interfejs.php');
					 	}
						else
						{
							$_SESSION['error']="Nieprawidłowe hasło!";
							header('Location: index.php');
							//echo "Nieprawidłowe hasło.";
						}
					}
					else
					{
						$_SESSION['error']="Nieprawidłowy login!";
						header('Location: index.php');
						//echo "Nieprawidłowy login";
					}
				}
				else
				{
					$_SESSION['error']="Wystąpił błąd, przepraszamy.!";
					header('Location: index.php');
					//echo "Wystąpił błąd, przepraszamy.";
				}
				$con->close();
			}
		}
	}
	echo json_encode($response);
?>