﻿<?php

	session_start();
    $con = @new mysqli("localhost", "root", "", "projekt2");

	
?>
<!--
<!DOCTYPE HTML>
<html lang="pl">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>Portal muzyczny</title>
</head>

<body>

	<p> Witaj ! <a href="logout.php">Wyloguj się!</a> </p>


</body>
</html>
-->
<style type="text/css">
		.card{
			width: 20rem;
			display: inline-block;					
			cursor: pointer;	
		}
		.card img{
		    position: relative;
    		width: 100%;
		}

		.my-temp li{
			cursor: pointer;
		    background-color: rgba(120, 120, 120, 0.6);
		    border: 1px solid rgba(180, 180, 180, 0.5);
		    color: rgba(180, 180, 180, 0.5);
		    transition: all 1s ease-in;
		}

		.my-temp img{
			z-index: -1;
		}

		.my-temp:hover li{
			background-color: rgba(120, 120, 120, 0.2);
		}

		.desc{
			transition: all 0.3s ease-in;
		}

		.desc:hover {		
			height: 150px !important;
		}


		.my-temp:hover .glyphicon{
			color: rgba(120, 120, 120, 0.6);
		}

		.my-temp .glyphicon {
		    position: absolute;
		    top: 80%;
		    left: 50%;
		    color: white;
		    font-size: 200%;
		    transform: translateX(-50%);
		    transition: all 1s ease-in;
		}
		.my-temp-new{
			display: none;
		}

		





		#image_div .img_wrapper{
		 width:180px;
		 position:relative;
		 display:inline-block;
		}

		#image_div .img_wrapper img{
		 width:100%;
		}

		#image_div .img_wrapper:hover img{
		 -webkit-filter: blur(1.7px);
		}

		#image_div .img_wrapper span{
		 display:none;
		 position:absolute;
		 top:65px;
		 left:30px;
		}

		#image_div .img_wrapper:hover span{
		 display:table-cell;
		}

		#image_div .img_wrapper span input[type="button"]{
		 width:120px;
		 height:40px;
		 background-color:#00BFFF;
		 border:none;
		 color:white;
		 font-weight:bold;
		 font-size:17px;
		}


	</style>

<html>
<head>
	<title>Portal Muzyczny</title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<script type="text/javascript">
		q$ = jQuery.noConflict();
	</script>
	<style type="text/css">
		body{
			overflow-x: hidden;
		}

		.logout{
			color: crimson !important;
		}

		a {
			cursor: pointer;
		}
	</style>
</head>
<body>	
	<nav class="navbar navbar-default">
		<div class="container-fluid">			
			<div class="navbar-header">
				<button type="button" 
						class="navbar-toggle collapsed"
						data-toggle="collapse" 
						data-target="#bs-example-navbar-collapse-1" 
						aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
				</button>
				<a class="navbar-brand" href="#">Portal muzyczny</a>
			</div>
			
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li id="catalog-bar">
						<a href="/catalog.php">									
							Tracks
						</a>
					</li>
					<li id="order-bar">
						<a href="/albums.php" />								   
							Albums
						</a>
					</li>
				</ul>
				<form  id="searchForm" action="catalog.php" method="POST" class="navbar-form navbar-left">	
					<div class="form-group">
						<input type="text" name="searchElem" class="form-control" placeholder="Search">
					</div>
					<button type="submit" class="btn btn-default">Submit</button>
				</form>
				<ul class="nav navbar-nav navbar-right">
					<!-- <li>
						<a href="#">Link</a>
					</li> -->
					<li class="dropdown">
						<a  href="#"
							class="dropdown-toggle"
							data-toggle="dropdown"
							role="button"
							aria-haspopup="true"
							aria-expanded="false">									
								<?php  						
									echo $_SESSION['login'];
								?>
							<span class="caret"></span>
						</a>
						<ul class="dropdown-menu">
							<li>
								<a href="#">
									<span class="glyphicon glyphicon-user"></span>
									My Profile
								</a>
							</li>
							<li>
								<a href="#">
									<span class="glyphicon glyphicon-cog"></span>
									Settings
								</a>
							</li>
							<li role="separator" class="divider">
							</li>
							<li>										
								<a href="logout.php" class="logout">
									<span class="glyphicon glyphicon-off"></span>
									Logout 
								</a>										
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</nav>
</body>
</html>
