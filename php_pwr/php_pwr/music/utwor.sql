-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 26 Maj 2018, 21:29
-- Wersja serwera: 10.1.31-MariaDB
-- Wersja PHP: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `projekt`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `utwor`
--

CREATE TABLE `utwor` (
  `id_utwor` bigint(20) UNSIGNED NOT NULL,
  `tytul` varchar(255) DEFAULT NULL,
  `id_album` int(11) DEFAULT NULL,
  `rok_powstania` char(4) DEFAULT NULL,
  `czas_trwania` time DEFAULT NULL,
  `mp3` longblob,
  `liczba_wyswietlen` int(11) DEFAULT NULL,
  `URLtoIMG` text NOT NULL,
  `PATHtoFILE` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `utwor`
--

INSERT INTO `utwor` (`id_utwor`, `tytul`, `id_album`, `rok_powstania`, `czas_trwania`, `mp3`, `liczba_wyswietlen`, `URLtoIMG`, `PATHtoFILE`) VALUES
(1, 'przykladowa', 1, '2018', '00:03:15', NULL, 0, 'https://t2.genius.com/unsafe/220x220/https%3A%2F%2Fimages.genius.com%2F7b3a041c99707417fa65ee89fc9ccfff.1000x1000x1.jpg', 'music/SampleAudio_0.4mb.mp3'),
(2, 'druga przykladowa', 2, '2017', '00:01:25', NULL, 0, 'https://img.cdandlp.com/2013/07/imgL/116077323.jpg', 'music/SampleAudio_0.7mb.mp3');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `utwor`
--
ALTER TABLE `utwor`
  ADD PRIMARY KEY (`id_utwor`),
  ADD UNIQUE KEY `id_utwor` (`id_utwor`),
  ADD KEY `FK` (`id_album`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `utwor`
--
ALTER TABLE `utwor`
  MODIFY `id_utwor` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
