<?php


?>



<html>
<head>
	<title>e-Shop PHP</title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<script type="text/javascript">
		q$ = jQuery.noConflict();
		
	</script>
	<style type="text/css">
		body{
			overflow-x: hidden;
		}

		.marginTop {
			margin-top: 15px;
		}

		.marginRight {
			margin-right: 5px;
		}

		.loginForm{
			margin: auto;
		    padding: 50px;
		    max-width: 450px;
		    background-color: white;
		    margin-top: 25px;
		    border-radius: 5px;
		    z-index: 2;
		}

		.loginForm .btn, h3 {
			color: #4f9fcf;
		}

		.loginForm .glyphicon {
			left: -54%;
			top: 30px;
		}

		.backEffect{
		    position: absolute;
		    margin: auto;
		    left: 0;
		    right: 0;
		    padding: 100px 210px;
		    max-width: 350px;
		    margin-top: 120px;
		    border-radius: 10px;
		    z-index: -1;
		    box-shadow: 0 0 100px 5px #4f9fcf;
		}
	</style>
</head>
<body>
	<div id="root" class="container-fluid">	
		<div class="row">
    		<div class="col-md-4 col-md-offset-4 marginTop">
    			<button type="button" class="close marginRight" data-dismiss="alert">&times;</button>
				          
				<div class="alert alert-danger">
					<strong>
						<center>
							<?php  		
								session_start();				
								echo $_SESSION['error'];
							?>
						</center>
					</strong>
							<!-- <strong>Danger!</strong> Indicates a dangerous or potentially negative action. -->
				</div>
			</div>
		</div>
			
	</div>
</body>
</html>