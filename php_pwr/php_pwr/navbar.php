<!DOCTYPE html>
<?php
	session_start(); 

	if(!isset($_SESSION['login'])){
		if(!isset($_POST['login'])){
			header("Location: /login.php");
		} else {
			$_SESSION['login'] = $_POST['login'];
			$_SESSION['pass'] = $_POST['pass'];
		}
	}

	$con = @mysqli_connect('localhost', $_SESSION['login'], $_SESSION['pass'],'sklep'); // or
	if(mysqli_connect_errno()){
		unset($_SESSION['login']);
		unset($_POST['login']);
		header("Location: /login.php?wrongLogin=1");			
	}
?>
<html>
<head>
	<title>e-Shop PHP</title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<script type="text/javascript">
		q$ = jQuery.noConflict();
	</script>
	<style type="text/css">
		body{
			overflow-x: hidden;
		}

		.logout{
			color: crimson !important;
		}

		a {
			cursor: pointer;
		}
	</style>
</head>
<body>	
	<nav class="navbar navbar-default">
		<div class="container-fluid">			
			<div class="navbar-header">
				<button type="button" 
						class="navbar-toggle collapsed"
						data-toggle="collapse" 
						data-target="#bs-example-navbar-collapse-1" 
						aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
				</button>
				<a class="navbar-brand" href="#">e-Shop PHP</a>
			</div>
			
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li id="catalog-bar" class="active">
						<a href="/catalog.php">									
							Catalog
						</a>
					</li>
					<li id="order-bar">
						<a href="/order.php" />								   
							Order
						</a>
					</li>
				</ul>
				<form  id="searchForm" action="catalog.php" method="POST" class="navbar-form navbar-left">	
					<div class="form-group">
						<input type="text" name="searchElem" class="form-control" placeholder="Search">
					</div>
					<button type="submit" class="btn btn-default">Submit</button>
				</form>
				<ul class="nav navbar-nav navbar-right">
					<!-- <li>
						<a href="#">Link</a>
					</li> -->
					<li class="dropdown">
						<a  href="#"
							class="dropdown-toggle"
							data-toggle="dropdown"
							role="button"
							aria-haspopup="true"
							aria-expanded="false">									
								<?php  						
									echo $_SESSION['login'];
								?>
							<span class="caret"></span>
						</a>
						<ul class="dropdown-menu">
							<li>
								<a href="#">
									<span class="glyphicon glyphicon-user"></span>
									My Profile
								</a>
							</li>
							<li>
								<a href="#">
									<span class="glyphicon glyphicon-cog"></span>
									Settings
								</a>
							</li>
							<li role="separator" class="divider">
							</li>
							<li>										
								<a href="/login.php?signOut=1" class="logout">
									<span class="glyphicon glyphicon-off"></span>
									Logout 
								</a>										
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</nav>
</body>
</html>
